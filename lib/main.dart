import 'package:covmotool/widget/COVSqlBuilder.dart';
import 'package:covmotool/widget/COVTextField.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'CovMo Tool',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        primaryColor: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter CovMo Tool'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController _controller;
  int _kpiId;

  @override
  void initState() {
    super.initState();
    _controller = TextEditingController();
    _controller.addListener(() {
      setState(() {
        _kpiId = int.tryParse(_controller.text);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView(
        children: [
          COVSqlBuilder(
            title: "Group name",
            hint: "Enter group name",
            filter: (text) => """
SELECT * FROM all_cov_kpi_group WHERE group_name LIKE '%$text%';
#group_id 
""",
          ),
          COVSqlBuilder(
            title: "Group id",
            hint: "Enter group id",
            filter: (text) => """
SELECT * FROM all_cov_kpi_group WHERE group_id IN ($text);
SELECT * FROM all_sys_group_author WHERE rpt_group_id IN ($text);
#tech 
""",
          ),
          COVSqlBuilder(
            title: "Kpi name",
            hint: "Enter kpi name",
            filter: (text) => """
SELECT * FROM all_cov_kpi WHERE kpi_name LIKE '%$text%';
#kpi_id 
""",
          ),
          COVSqlBuilder(
            title: "Kpi id",
            hint: "Enter kpi id",
            filter: (text) => """
SELECT * FROM all_cov_kpi WHERE kpi_id IN ($text);
SELECT * FROM all_cov_kpi_behavior WHERE kpi_id IN ($text);
#report_cell 
#report_tile 
#report_imsi 
#report_call 
#map 
#hichart 
""",
          ),
          COVSqlBuilder(
            title: "Report id trace up",
            hint: "Enter report id",
            filter: (text) => """
SELECT * FROM all_cov_kpi_behavior WHERE id IN ($text) AND action_type LIKE \"report_%\";
SELECT * FROM all_cov_rpt_behavior WHERE id IN ($text) AND action_type LIKE \"report_%\";
#report_imsi 
#report_call 
#map 
#hichart 
""",
          ),
          COVSqlBuilder(
            title: "Report id drill down",
            hint: "Enter report id",
            filter: (text) => """
SELECT * FROM all_cov_rpt_behavior WHERE report_id IN ($text) AND
action_type IN ('report_cell', 'report_tile', 'report_imsi', 'report_call', 'map', 'hichart');
SELECT * FROM all_cov_rpt WHERE report_id IN ($text);
SELECT * FROM all_cov_rpt_col WHERE report_id IN ($text);
#report_imsi 
#report_call 
#map 
#hichart 
""",
          ),
          COVSqlBuilder(
            title: "Map id",
            hint: "Enter map id",
            filter: (text) => """
SELECT * FROM all_cov_rpt_behavior WHERE id IN ($text) and action_type = 'map';
SELECT * FROM all_cov_map WHERE map_id IN ($text);
#legend_id 
""",
          ),
          COVSqlBuilder(
            title: "HiChart id",
            hint: "Enter HiChart id",
            filter: (text) => """
SELECT * FROM all_cov_rpt_behavior WHERE id IN ($text) and action_type = 'hichart';
SELECT * FROM all_cov_hichart WHERE hichart_id IN ($text);
SELECT * FROM all_cov_hichart_behavior WHERE chart_id IN ($text);
SELECT * FROM all_cov_hichart_series_experimental WHERE hichart_id IN ($text);
""",
          ),
          COVSqlBuilder(
            title: "legend id",
            hint: "Enter legend id",
            filter: (text) => """
SELECT * FROM all_cov_legend_master WHERE legend_id IN ($text);
""",
          ),
        ],
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
