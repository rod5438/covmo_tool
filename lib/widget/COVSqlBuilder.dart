import 'package:covmotool/widget/COVTextField.dart';
import 'package:flutter/material.dart';

class COVSqlBuilder extends StatefulWidget {
  final String title;
  final String hint;
  final String Function(String) filter;

  COVSqlBuilder({Key key, this.title, this.hint, this.filter}) : super(key: key);

  @override
  _COVSqlBuilderState createState() => _COVSqlBuilderState();
}

class _COVSqlBuilderState extends State<COVSqlBuilder> {
  TextEditingController _controller;
  String outputText;

  @override
  void initState() {
    super.initState();
    _controller = TextEditingController();
    _controller.addListener(() {
      setState(() {
        outputText = _controller.text.length > 0 ? widget.filter?.call(_controller.text) : null;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(5),
      padding: EdgeInsets.all(5),
      decoration: BoxDecoration(
        border: Border.all(
          width: 2,
          color: Theme.of(context).primaryColor,
        ),
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Container(
              padding: EdgeInsets.only(right: 5),
                child: Text(
              widget.title,
              textAlign: TextAlign.right,
            )),
            flex: 1,
          ),
          Expanded(
            child: TextField(
              controller: _controller,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: widget.hint,
              ),
            ),
            flex: 2,
          ),
          Expanded(
            child: COVTextField(
              value: outputText,
            ),
            flex: 10,
          ),
        ],
      ),
    );
  }
}
