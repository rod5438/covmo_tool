import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:covmotool/foundation/COVString.dart';

class COVTextField extends StatelessWidget {
  final String value;

  COVTextField({Key key, this.value}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: [25 * (value?.lines() ?? 1).toDouble(), 60].reduce((value, element) => value > element ? value : element),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          IconButton(
            icon: Icon(Icons.content_copy),
            onPressed: () {
              Clipboard.setData(ClipboardData(text: value));
            },
          ),
          Expanded(
              child: TextField(
            decoration: InputDecoration(
              border: OutlineInputBorder(),
            ),
            maxLines: value?.lines(),
            controller: TextEditingController()..text = value,
            readOnly: true,
          )),
        ],
      ),
    );
  }
}
