extension COVString on String {
  int lines() => '\n'.allMatches(this).length + 1;
}
